# Docker Multiarch Ejemplo

Este es un sencillo ejemplo de como empaquetar una aplicación en Docker para multiples arquitecturas de CPU (amd64, arm64, 386, etc) utilizando herramientas como builx y trayendo más diversión con GitLab CI

Complemento del artículo en mi blog:

<https://blog.enmanuelmoreira.com/crear-imagenes-multi-arquitectura-de-docker-con-buildx>

# Licencia
- MIT

# Autor
[Enmanuel Moreira](https://enmanuelmoreira.com)
